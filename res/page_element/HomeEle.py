# coding:utf-8
from selenium.webdriver.common.by import By

class Home:
    LOGIN = (By.XPATH, '//span[@class = "nav-line-1" and contains(text(), "Sign in")]')
    BANNER = (By.XPATH, '//div[@class = "a-section a-spacing-none shogun-widget asin-shoveler aui-desktop fresh-shoveler"]')
