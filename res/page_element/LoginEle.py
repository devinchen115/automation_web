# coding:utf-8
from selenium.webdriver.common.by import By

class Login:
    EMAIL = (By.XPATH, '//input[@id = "ap_email"]')
    PASSWORD = (By.XPATH, '//input[@id = "ap_password"]')
    SIGN_IN = (By.XPATH, '//input[@id = "signInSubmit"]')
    EMAIL_ALERT = (By.XPATH, '//div[@id = "auth-email-missing-alert"]')
    PASSWORD_ALERT = (By.XPATH, '//div[@id = "auth-password-missing-alert"]')
    IMPORTANT_MESSAGE_TITLE = (By.XPATH, '//h4[@class = "a-alert-heading"]')
    IMPORTANT_MESSAGE_CONTENT = (By.XPATH, '//ul[@class = "a-unordered-list a-nostyle a-vertical a-spacing-none"]')
