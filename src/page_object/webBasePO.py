# coding:utf-8
import os
import sys
import time
import traceback

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


project_root = os.getenv('WORKSPACE') if os.getenv('WORKSPACE') is not None else str(os.path.abspath(__file__).split('automation_web')[0]) + 'automation_web'
sys.path.append(project_root)

from lib import utils
from res.config import time_out as general
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.select import Select

today = time.strftime("%Y%m%d")

class GetDriver:
    @staticmethod
    def get_driver(browser=None, log=False, download_path=""):

        if browser is None:
            browser = 'chrome'

        current_os = utils.get_current_os()
        chrome_desired_capabilities = None
        ##Turn on log if need
        if log is True:
            chrome_desired_capabilities = DesiredCapabilities.CHROME
            chrome_desired_capabilities['loggingPrefs'] = {'performance': 'ALL'}

        if browser == 'chrome':
            options = Options()
            prefs = {'profile.default_content_settings.popups': 0, 'download.default_directory': download_path, 'intl.accept_languages': 'en-US'}
            options.add_experimental_option('prefs', prefs)
            options.add_argument("--disable-application-cache")

            if current_os == 'Mac':
                chrome_driver_path = os.path.join(project_root, 'res', 'browser', 'chromedriver')
                chrome_driver = webdriver.Chrome(chrome_options=options, executable_path=chrome_driver_path,
                                                 desired_capabilities=chrome_desired_capabilities)
                return chrome_driver
            elif current_os == 'Windows':
                chrome_driver_path = os.path.join(project_root, 'res', 'browser', 'chromedriver.exe')
                chrome_driver = webdriver.Chrome(chrome_options=options, executable_path=chrome_driver_path,
                                                 desired_capabilities=chrome_desired_capabilities)
                chrome_driver.maximize_window()
                return chrome_driver

        elif browser == 'firefox':
            if current_os == 'Mac':
                firefox_driver_path = os.path.join(project_root, 'res', 'browser', 'geckodriver')
                profile = webdriver.FirefoxProfile()
                profile.set_preference("intl.accept_languages", "us")
                firefox_driver = webdriver.Firefox(executable_path=firefox_driver_path, firefox_profile=profile)
                firefox_driver.maximize_window()
                return firefox_driver
            elif current_os == 'Windows':
                firefox_driver_path = os.path.join(project_root, 'res', 'browser', 'geckodriver.exe')
                profile = webdriver.FirefoxProfile()
                profile.set_preference("intl.accept_languages", "us")
                firefox_driver = webdriver.Firefox(executable_path=firefox_driver_path)
                firefox_driver.maximize_window()
                return firefox_driver

        elif browser == 'safari':
            if current_os == 'Mac':
                safari_driver = webdriver.Safari()
                safari_driver.maximize_window()
                return safari_driver


class WebPageObject(object):
    def __init__(self, driver):
        self.browser = driver

    def go_to_website(self, uri):
        utils.log('go to website: %s' % uri)
        try:
            self.browser.get(uri)
        except Exception as e:
            utils.log('go to website: %s Failed' % uri)
            raise e

    def is_find_element(self, locator, log='',poll_frq=10, timeout=general.web_timeout, is_visible=True):
        try:
            self.find_element(locator, log, timeout, is_visible)
            return True
        except:
            utils.log('Find element Failed')
            return False


    def find_element(self, locator, log='', timeout=general.web_timeout, is_visible=True):
        if log == '':
            log = utils.gen_find_log(locator)

        try:
            if is_visible:
                element = WebDriverWait(self.browser, timeout).until(EC.visibility_of_element_located(locator), log)
                utils.log(log, 'Done')
                return element
            else:
                element = WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located(locator), log)
                utils.log(log, 'Done')
                return element
        except Exception as e:
            traceback.print_exc()
            raise e

    def find_elements(self, locator, log='', timeout=general.web_timeout, is_visible=True):
        if log == '':
            log = utils.gen_find_log(locator)

        try:
            if is_visible:
                elements = WebDriverWait(self.browser, timeout).until(EC.visibility_of_all_elements_located(locator),
                                                                      log)
                utils.log(log, 'Done')
                return elements
            else:
                elements = WebDriverWait(self.browser, timeout).until(EC.presence_of_all_elements_located(locator), log)
                utils.log(log, 'Done')
                return elements
        except Exception as e:
            utils.color_print('%s error' % str(time.time()))
            traceback.print_exc()
            raise e

    def assert_exits_by_airtest(self, locator,threshold=0.70):
        from airtest.core.api import Template
        time.sleep(general.default_timeout)
        self.browser.assert_template(Template(locator[1],threshold=threshold,record_pos=locator[2], resolution=locator[3]))

    def click_element(self, locator, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator, 'Click')

        try:
            element = WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located(locator), log)
            element.click()
            utils.log(log, 'Done')
        except:
            try:
                element = WebDriverWait(self.browser, timeout).until(EC.element_to_be_clickable(locator), log)
                element.click()
                utils.log(log, 'Done')
            except:
                # utils.color_print('use javascript to click element')
                element = WebDriverWait(self.browser, timeout).until(EC.visibility_of_element_located(locator), log)
                self.browser.execute_script("arguments[0].click();", element)
                utils.log(log, 'Done')

    def get_elements_text(self, locator, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator)
        try:
            elements = WebDriverWait(self.browser, timeout).until(EC.visibility_of_all_elements_located(locator), log)
            utils.log('find %s elements of %s' % (len(elements), locator), 'Done')
            elements_text = []
            for i in range(len(elements)):
                elements_text.insert(i, elements[i].text)

            return elements_text

        except Exception as e:
            traceback.print_exc()
            raise e

    def get_element_text(self, locator, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator, 'Get')
        try:
            element = WebDriverWait(self.browser, timeout).until(EC.visibility_of_element_located(locator), log)
            utils.log(log, 'Done')
            return element.text

        except Exception as e:
            traceback.print_exc()
            raise e

    def is_element_attr_exist(self, locator, attr, keyword, log='', timeout=general.web_timeout):
        attr_value = self.get_element_attr(locator, attr, log, timeout)
        if keyword in attr_value:
            return True
        else:
            return False

    def get_element_attr(self, locator, attr, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator, 'Get-attr')
        try:
            element = WebDriverWait(self.browser, timeout).until(EC.visibility_of_element_located(locator), log)
            attr_value = element.get_attribute(attr)
            utils.log(log, 'Done')
            utils.log('attr of %s is: %s' % (attr, attr_value))
            return attr_value

        except Exception as e:
            traceback.print_exc()
            raise e

    def get_elements_attr(self, locator, attr, log='', timeout=general.web_timeout):
        result = list()
        if log == '':
            log = utils.gen_find_log(locator, 'Get-attr')
        try:
            elements = WebDriverWait(self.browser, timeout).until(EC.visibility_of_all_elements_located(locator), log)
            utils.log(log, 'Done')
            for element in elements:
                result.append(element.get_attribute(attr))
            return result

        except Exception as e:
            traceback.print_exc()
            raise e

    def fill_element_data(self, locator, text, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator, 'Fill')
        try:
            element = self.find_element(locator, log)
            element.send_keys(text)
            utils.log(log, 'Done')
        except Exception as e:
            traceback.print_exc()
            raise e

    def fill_element_data_action_chins(self, locator, text, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator, 'Fill')
        try:
            element = self.find_element(locator, log, timeout)
            action = ActionChains(self.browser)
            action.click(element)
            action.send_keys(text)
            action.perform()
        except Exception as e:
            traceback.print_exc()
            raise e

    def click_element_action_chins(self, locator, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator, 'Click')

        try:
            element = self.find_element(locator, log, timeout)
            action = ActionChains(self.browser)
            action.click(element)
            utils.log(log, 'Done')
        except Exception as e:
            traceback.print_exc()
            raise e

    def double_click_element_action_chins(self, locator, log='', timeout=general.web_timeout):
        if log == '':
            log = utils.gen_find_log(locator, 'Click')

        try:
            element = self.find_element(locator, log, timeout)
            action = ActionChains(self.browser)
            action.double_click(element).perform()
            utils.log(log, 'Done')
        except Exception as e:
            traceback.print_exc()
            raise e

    def screenshot(self, name):
        screen_dir = os.path.join(project_root, 'screenshot', today)
        utils.check_folder(screen_dir)
        try:
            timestamp = str(time.strftime("%Y%m%d-%H%M%S"))
            screenshot_name = timestamp + "_" + name + ".png"
            utils.log("Taking screenshot: " + screen_dir + screenshot_name)
            self.browser.get_screenshot_as_file(screen_dir + screenshot_name)
        except Exception as e:
            print(e)

    def set_text(self, locator, text, log='', timeout=general.web_timeout):
        """
        clear data -> fill in text in the text box
        :param locator: specific PageElement
        :param text: text to be enter
        """
        if log == '':
            log = utils.gen_find_log(locator, 'Clear-and-Fill')

        element = WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located(locator), log)

        element.send_keys(text)
        utils.log(log, 'Done')
        utils.log('Input text: %s' % text)

    def open_new_tab(self, url='https://www.google.com', waiting_time=1):
        self.browser.execute_script("window.open('%s');" % url)
        windows = self.browser.window_handles
        self.browser.switch_to.window(windows[len(windows)-1])
        time.sleep(waiting_time)

    def switch_to_tab(self, tab=0):
        windows = self.browser.window_handles
        self.browser.switch_to.window(windows[tab])

    def web_page_refresh(self):
        self.browser.refresh()

    def get_page_content_size(self):
        """
        get the size of the whole page content
        :return:
        """
        height = self.browser.execute_script('return $(document).height()')
        width = self.browser.execute_script('return $(document).width()')
        return height, width

    def get_browser_size(self):
        """
        get the inner size of the browser (the size of the "visible" browser content)
        :return:
        """
        height = self.browser.execute_script('return window.innerHeight')
        width = self.browser.execute_script('return window.innerWidth')
        return height, width