# coding:utf-8
import os
import sys

project_root = os.getenv('WORKSPACE') if os.getenv('WORKSPACE') is not None else str(os.path.abspath(__file__).split('automation_web')[0]) + 'automation_web'
sys.path.append(project_root)

from res.page_element.LoginEle import Login
from src.page_object.webBasePO import WebPageObject
from lib import utils


class LoginPage(WebPageObject):
    def is_in_login_page(self):
        try:
            self.find_element(Login.EMAIL)
            self.find_element(Login.PASSWORD)
            self.find_element(Login.SIGN_IN)
            return True
        except Exception as e:
            return False

    def do_login(self, email, pwd):
        try:
            self.fill_element_data(Login.EMAIL, email)
            self.fill_element_data(Login.PASSWORD, pwd)
            self.click_element(Login.SIGN_IN)
        except Exception as e:
            utils.log('login', 'Fail')
            raise e

    def get_import_message(self):
        try:
            title = self.get_element_text(Login.IMPORTANT_MESSAGE_TITLE)
            content = self.get_element_text(Login.IMPORTANT_MESSAGE_CONTENT)
            return [title.strip(), content.strip()]
        except Exception as e:
            utils.log('get import message', 'Fail')
            raise e

    def get_email_alert(self):
        try:
            email_alert = self.get_element_text(Login.EMAIL_ALERT)
            return email_alert.strip()
        except Exception as e:
            utils.log('get email alert', 'Fail')
            raise e

    def get_password_alert(self):
        try:
            password_alert = self.get_element_text(Login.PASSWORD_ALERT)
            return password_alert.strip()
        except Exception as e:
            utils.log('get email alert', 'Fail')
            raise e
