# coding:utf-8
import os
import sys

project_root = os.getenv('WORKSPACE') if os.getenv('WORKSPACE') is not None else str(os.path.abspath(__file__).split('automation_web')[0]) + 'automation_web'
sys.path.append(project_root)

from res.page_element.HomeEle import Home
from src.page_object.webBasePO import WebPageObject
from lib import utils


class HomePage(WebPageObject):
    def is_in_home_page(self):
        return self.is_find_element(Home.BANNER)

    def go_to_login_page(self):
        try:
            self.click_element(Home.LOGIN)
        except:
            utils.log('Go to login page', 'Fail')
