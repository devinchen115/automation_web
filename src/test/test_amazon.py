# coding:utf-8
import os
import sys
import traceback
import unittest
import time


project_root = os.getenv('WORKSPACE') if os.getenv('WORKSPACE') is not None else str(os.path.abspath(__file__).split('automation_web')[0]) + 'automation_web'
sys.path.append(project_root)

from res.config import time_out
from lib import utils
from lib import HTMLTestRunner
from src.page_object.webBasePO import WebPageObject
from src.page_object.webBasePO import GetDriver
from src.page_object.HomePO import HomePage
from src.page_object.LoginPO import LoginPage


class Login(unittest.TestCase, WebPageObject):

    @classmethod
    def setUpClass(cls):
        cls.browser = GetDriver().get_driver()
        cls.home_page = HomePage(cls.browser)
        cls.login_page = LoginPage(cls.browser)
        cls.login_id = 'test.for.amazon.shopback@gmail.com'
        cls.login_pwd = 'ABCD123$'
        cls.login_id_wrong = 'test.for.amazon.shopbaaaack@gmail.com'
        cls.login_pwd_wrong = '1234abcd'
        cls.email_alert = 'Enter your email or mobile phone number'
        cls.password_alert = 'Enter your password'
        cls.import_message_content_wrong_title = 'There was a problem'
        cls.import_message_content_wrong_id = 'We cannot find an account with that email address'
        cls.import_message_content_worng_pwd = 'Your password is incorrect'

    @classmethod
    def tearDownClass(cls):
        cls().browser.quit()

    def setUp(self):
        try:
            self.go_to_website('https://www.amazon.com')
        except Exception as e:
            traceback.print_exc()
            self.screenshot(self.__class__.__name__ + '_' + self._testMethodName)
            self.fail(str(e))

    def tearDown(self):
        print('testDown')


    def test_login_with_no_data(self):
        expect_result = [self.email_alert, self.password_alert]
        try:
            self.assertTrue(self.home_page.is_in_home_page())
            self.home_page.go_to_login_page()
            self.assertTrue(self.login_page.is_in_login_page())
            self.login_page.do_login('', '')
            email_alert = self.login_page.get_email_alert()
            password_alert = self.login_page.get_password_alert()
            self.assertEqual(expect_result, [email_alert, password_alert])

        except Exception as e:
            self.fail(e)

    def test_login_with_wrong_id(self):
        expect_result = [self.import_message_content_wrong_title, self.import_message_content_wrong_id]
        try:
            self.assertTrue(self.home_page.is_in_home_page())
            self.home_page.go_to_login_page()
            self.assertTrue(self.login_page.is_in_login_page())
            self.login_page.do_login(self.login_id_wrong, self.login_pwd)
            import_message = self.login_page.get_import_message()
            self.assertEqual(expect_result, import_message)

        except Exception as e:
            self.fail(e)

    def test_login_with_wrong_pwd(self):
        expect_result = [self.import_message_content_wrong_title, self.import_message_content_worng_pwd]
        try:
            self.assertTrue(self.home_page.is_in_home_page())
            self.home_page.go_to_login_page()
            self.assertTrue(self.login_page.is_in_login_page())
            self.login_page.do_login(self.login_id, self.login_pwd_wrong)
            import_message = self.login_page.get_import_message()
            self.assertEqual(expect_result, import_message)

        except Exception as e:
            self.fail(e)

    def test_login_with_wrong_id_and_pwd(self):
        expect_result = [self.import_message_content_wrong_title, self.import_message_content_wrong_id]
        try:
            self.assertTrue(self.home_page.is_in_home_page())
            self.home_page.go_to_login_page()
            self.assertTrue(self.login_page.is_in_login_page())
            self.login_page.do_login(self.login_id_wrong, self.login_pwd_wrong)
            import_message = self.login_page.get_import_message()
            self.assertEqual(expect_result, import_message)

        except Exception as e:
            self.fail(e)

    def test_login_z_success(self):
        from res.page_element.HomeEle import Home
        try:
            self.assertTrue(self.home_page.is_in_home_page())
            self.home_page.go_to_login_page()
            self.assertTrue(self.login_page.is_in_login_page())
            self.login_page.do_login(self.login_id, self.login_pwd)
            self.assertFalse(self.is_find_element(Home.LOGIN, timeout=time_out.default_timeout))

        except Exception as e:
            self.fail(e)




if __name__ == '__main__':
    debug_mode = os.getenv('SHOPBACK_DEBUG') if os.getenv('SHOPBACK_DEBUG') is not None else 'N'
    today = time.strftime("%Y%m%d")

    loader = unittest.TestLoader()
    suite = unittest.TestSuite((
        loader.loadTestsFromTestCase(Login)
    ))

    if debug_mode == 'Y':
        unittest.TextTestRunner(verbosity=2).run(suite)
    else:
        report_folder = os.path.join(project_root, 'report', today)
        utils.check_folder(report_folder)
        file_name = os.getenv('REPORT_NAME') if os.getenv('REPORT_NAME') is not None else 'SHOPBACK_QA'
        report_path = os.path.join(report_folder, '%s_%s.html' % (file_name, time.strftime("%Y%m%d-%H%M%S")))
        file = open(report_path, "wb")

        runner = HTMLTestRunner.HTMLTestRunner(
            stream=file,
            title="amazon login",
            description='This test script focus on login only (no need to consider sign-up, logout)'
        )
        runner.run(suite)
        file.close()
