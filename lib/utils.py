import inspect
import os
import platform
import random
import string
import sys
import time


project_root = os.getenv('WORKSPACE') if os.getenv('WORKSPACE') is not None else str(os.path.abspath(__file__).split('automation_web')[0]) + 'automation_web'
sys.path.append(project_root)


def normalizeStringToList(object):
    output = object
    if isinstance(output, list):
        pass
    else:
        output = [str(object)]
    return output


# execute os command
def osCommand(cmd):
    pyVersion = str(sys.version_info)
    if 'major=2' in pyVersion:
        import commands
        return commands.getoutput(cmd)
    else:
        import subprocess
        return subprocess.getoutput(cmd)


def log(msg, type=''):
    try:
        if msg != "" and type != '':
            str = '{0:-<80}'.format(time.strftime("%H:%M:%S") + ": " + msg)
            print(str + '-' + get_color_text(type))
        elif msg != "" and type == '':
            print(time.strftime("%H:%M:%S") + ": " + msg)
    except Exception as e:
        print(e)

def get_color_text(msg):
    return '\033[95m %s \033[0m' % msg


def debugLog(type, msg):
    try:
        if type == 'Y' or 'OneCase' or 'OneSuite':
            log('[Debug] - ' + str(msg))
    except Exception as e:
        print(e)

def check_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)


def isOSWindows():
    osPlatform = platform.system()
    if osPlatform.lower() == 'windows':  # windows
        return True
    elif osPlatform.lower() == 'linux':  # ubuntu
        return False
    elif osPlatform.lower() == 'darwin':  # mac
        return False


def requirefile(filepath):
    """provide a function to check the file existence

    Args:
        filepath: full path of the file to be checked
    """
    try:
        if os.path.exists(filepath):
            log('checking of %s ok' % filepath, 'Info')

        else:
            raise Exception

    except Exception as e:
        log('checking of required file fail: %s' % filepath, 'Fail')
        raise e
    else:
        pass


def deleteEnv(env):
    if env in os.environ:
        os.environ.pop(env)



def color_print(src):
    print('\033[95m  %s  \033[0m' % str(src))


def gen_find_log(element, action='Find'):
    '''
    :param element: what string you want to display
    :param action: [Find(default) | Get | Click]
    :return: action + string for display
    '''
    get_text_log = 'Get text of'
    get_desc_log = 'Get content-desc of'
    get_selected_log = 'Get selected attribute of'
    get_checked_log = 'Get checked attribute of'
    get_clickable_log = 'Get clickable attribute of'
    get_enabled_log = 'Get enabled attribute of'
    get_attr_log = 'Get attribute of'
    click_log = 'Click'
    set_text_log = 'Set text to'
    multi_find_log = 'Multi-Find'
    move_to_click_log = 'Move to click'
    multi_get_texts_log = 'Multi-Get texts of'
    scroll_up_log = 'Scroll up to'
    scroll_down_log = 'Scroll down to'
    switch_to_frame_log = 'Switch to iframe'
    send_multiple_tab_log = 'Send multiple TAB'
    clear_and_fill_in_log = 'Clear and fill in'

    if action == 'Get':
        action = get_text_log
    elif action == 'Get-desc':
        action = get_desc_log
    elif action == 'Get-selected':
        action = get_selected_log
    elif action == 'Get-checked':
        action = get_checked_log
    elif action == 'Get-clickable':
        action = get_clickable_log
    elif action == 'Get-enabled':
        action = get_enabled_log
    elif action == 'Get-attr':
        action = get_attr_log
    elif action == 'Click':
        action = click_log
    elif action == 'Set-text':
        action = set_text_log
    elif action == 'Move-To-Click':
        action = move_to_click_log
    elif action == 'Multi-Find':
        action = multi_find_log
    elif action == 'Multi-Get':
        action = multi_get_texts_log
    elif action == 'Scroll-Up':
        action = scroll_up_log
    elif action == 'Scroll-Down':
        action = scroll_down_log
    elif action == 'Fill':
        action = 'Fill data in'
    elif action == 'Clear-and-Fill':
        action = clear_and_fill_in_log
    elif action == 'Switch-Iframe':
        action = switch_to_frame_log
    elif action == 'send_multi_tab':
        action = send_multiple_tab_log

    else:
        pass
    stack = inspect.stack()
    the_class = stack[1][0].f_locals["self"].__class__
    the_class = str(the_class).replace('<', '').replace('>', '').replace('\'', '')
    class_name = the_class.split('.')
    return '%s %s from %s.%s' % (action, element, class_name[-2], class_name[-1])


def get_current_os():
    current_os = platform.system()
    if current_os == 'Darwin':
        return 'Mac'
    else:
        return current_os


def get_dynamic_locator(locator_with_placeholder, list_data):
    """
    fill the placeholder xpath with test data
    :param locator_with_placeholder: target PageElement
    :param list_data: list of data to be filled in the placeholder
    :return:
    """
    return_value = 0
    if len(locator_with_placeholder) == 2:
        return_value = (locator_with_placeholder[0], locator_with_placeholder[1] % tuple(list_data))
    return return_value


def get_random_characters(length=8):
    str_list = [random.choice(string.digits + string.ascii_lowercase) for i in range(length)]
    random_str = ''.join(str_list)
    return random_str


def get_random_numbers(lower_bound=1, upper_bound=10, sample=1):
    """
    get random number from a range, return None if range of number is smaller than 0 or sample is negative number
    default: get 1 random number from 1 to 10 inclusively
    :param lower_bound: lowest possible value
    :param upper_bound: largest possible value
    :param sample: samples size,[>0]
    :return: list of random number in ascending order or None
    """
    if upper_bound + 1 - lower_bound <= 0 or sample <= 0:
        # invalid boundary
        return None
    elif upper_bound + 1 - lower_bound < sample:
        # return all number in range if sample is larger than the possible range if number
        sample = upper_bound + 1 - lower_bound
    return sorted(random.sample(range(lower_bound, upper_bound + 1), sample))


# Generate random string
def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))
